package com.java.ds.springbootconsole;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class SpringbootconsoleApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootconsoleApplication.class, args);
	}

	@Override
	public void run(String... args) {
		String a = "1";
		String b = "1";
		log.info("{}", a.equals(b));
	}

}
