package com.java.ds.springbootwebandjdbc.controllers;

import com.java.ds.springbootwebandjdbc.models.User;
import com.java.ds.springbootwebandjdbc.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
public class UserController {

    private final UserService userService;

    @GetMapping(value = "/list")
    public ResponseEntity<List<User>> listUsers() {
        try {
            List<User> users = userService.listAllUser();
            if (CollectionUtils.isEmpty(users)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
