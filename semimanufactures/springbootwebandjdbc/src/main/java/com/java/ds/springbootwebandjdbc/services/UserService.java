package com.java.ds.springbootwebandjdbc.services;

import com.java.ds.springbootwebandjdbc.models.User;
import com.java.ds.springbootwebandjdbc.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    public List<User> listAllUser() {
        return userRepository.findAll();
    }
}
