package com.java.ds.springbootwebandjdbc.repositories;

import com.java.ds.springbootwebandjdbc.models.User;
import java.util.List;

public interface UserRepository {
    List<User> findAll();
}
