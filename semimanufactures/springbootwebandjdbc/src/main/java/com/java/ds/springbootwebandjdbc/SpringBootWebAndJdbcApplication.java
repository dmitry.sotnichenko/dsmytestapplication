package com.java.ds.springbootwebandjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebAndJdbcApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootWebAndJdbcApplication.class, args);
    }
}
