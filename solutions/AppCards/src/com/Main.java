package com;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        if (args.length > 0) {
            Service service = new Service();
            Path path = Paths.get(args[0]);

            if (Files.notExists(path)) {
                System.err.println("Path to images not found");
                System.exit(0);
            }

            try (Stream<Path> paths = Files.walk(path)) {
                System.out.println(new Date());
                paths.filter(Files::isRegularFile)
                        .filter(service::checkExtension)
                        .forEach(service::processing);
                System.out.println(new Date());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Required parameter - path to images");
        }
    }
}
