package com;

public class Common {
    /**
     * Первая карта позиция X
     */
    public static final short START_X = 147;
    /**
     * Первая карта позиция Y
     */
    public static final short START_Y = 590;
    /**
     * Масть первой карты позиция Y
     */
    public static final short START_Y_SUIT = 615;
    /**
     * Шаг межу картами
     */
    public static final short CARD_STEP = 72;
    /**
     * Сторона квадрата с символом
     */
    public static final short HW = 28;
    /**
     * Сторона квадрата с мастью
     */
    public static final short HW_SUIT = 25;
    /**
     * RGB белого цвета
     */
    public static final int WHITE_RBG = -1;
    /**
     * RGB серого цвета
     */
    public static final int GREY_RBG = -8882056;


    public static final String IMAGE_EXTENSION = "PNG";

}
