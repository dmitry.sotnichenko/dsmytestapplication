package com;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;


public class Service {

    /**
     * Пероцессинг картинок
     * @param path - путь до папки с файлами
     */
    public void processing(Path path) {
        try  {
            System.out.printf("%s - ", path.getFileName().toString());

            BufferedImage img = ImageIO.read(path.toFile());

            int x = Common.START_X;
            while (img.getRGB(x, Common.START_Y) == Common.WHITE_RBG || img.getRGB(x, Common.START_Y) == Common.GREY_RBG) {

                BufferedImage symbol = img.getSubimage(x, Common.START_Y, Common.HW, Common.HW);
                System.out.print(findSymbol(symbolToLine(symbol, Common.HW)));

                BufferedImage suit = img.getSubimage(x, Common.START_Y_SUIT, Common.HW_SUIT, Common.HW_SUIT);
                System.out.print(findSymbol(symbolToLine(suit, Common.HW_SUIT)));

                x = x + Common.CARD_STEP;
            }
            System.out.println();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param symbol картинка с одним символом
     * @param maxLength максимальная длина/ширина картинки
     * @return возвращает картинку как бинарную строку символов
     */
    private String symbolToLine(BufferedImage symbol, short maxLength) {
        StringBuilder binaryString = new StringBuilder();
        for (short y = 1; y < maxLength; y++)
            for (short x = 1; x < maxLength; x++) {
                int rgb = symbol.getRGB(x, y);
                binaryString.append((rgb == Common.WHITE_RBG || rgb == Common.GREY_RBG) ? " " : "*");
            }
        return binaryString.toString();
    }

    /**
     *
     * @param binaryString картинка как бинарная строка символов
     * @return Искомый символ как строка
     */
    private String findSymbol(String binaryString) {
        int min = 1000000;
        OriginalMap originalMap = new OriginalMap();

        String findSymbol = "";
        for (Map.Entry<String, String> entry : originalMap.getOriginalMap().entrySet()) {
            int levenshtein = levenshtein(binaryString, entry.getValue());
            if (levenshtein < min) {
                min = levenshtein;
                findSymbol = entry.getKey();
            }
        }
        return findSymbol;
    }

    /**
     * Реализация алгоритма Левенштейна
     * @param targetStr картинка которую определяем как бинарная строка символов
     * @param sourceStr картинка с которой сравниваем как бинарная строка символов
     * @return коофициент соответствия
     */
    private int levenshtein(String targetStr, String sourceStr) {
        int m = targetStr.length(), n = sourceStr.length();
        int[][] delta = new int[m + 1][n + 1];
        for (int i = 1; i <= m; i++)
            delta[i][0] = i;
        for (int j = 1; j <= n; j++)
            delta[0][j] = j;
        for (int j = 1; j <= n; j++)
            for (int i = 1; i <= m; i++) {
                if (targetStr.charAt(i - 1) == sourceStr.charAt(j - 1))
                    delta[i][j] = delta[i - 1][j - 1];
                else
                    delta[i][j] = Math.min(delta[i - 1][j] + 1,
                            Math.min(delta[i][j - 1] + 1, delta[i - 1][j - 1] + 1));
            }
        return delta[m][n];
    }

    /**
     * Вычисляем расширение файла
     * @param path файл
     * @return расширение
     */
    public boolean checkExtension(Path path) {
        String filename = path.getFileName().toString();
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1))
                .map(String::toUpperCase)
                .stream().anyMatch(Common.IMAGE_EXTENSION::equals);
    }
}
